﻿<?xml version="1.0" encoding="utf-8"?>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd" MadCap:conditions="Default.Not Ready for Publish">
    <head>
    </head>
    <body>
        <p class="labelKB">Knowledge Base</p>
        <h1 class="nobreak">Configuring AD Connectors to Support Multiple Child Tenants</h1>
        <h2>Summary</h2>
        <p>Some enterprise customers deploy their own VERA Cloud environments in AWS. These environments consist of a master tenant and multiple child tenants. The child tenants often serve separate divisions within the enterprise. To reduce the number of AD Connector clusters that need to be deployed, VERA enables these clusters to serve multiple child tenants. This article details how to deploy AD Connectors to serve multiple child tenants.</p>
        <h2><a name="Auto-gen"></a>Auto-generated Aliases</h2>
        <p>To support multiple tenants for a single Connector cluster, VERA automatically generates the aliases that point to the AD Connector from the child tenants based on the following format:</p>
        <p>&lt;tenant_name&gt;-&lt;Connector_FQDN&gt;</p>
        <p>Consider the following deployment:</p>
        <p>
            <img src="../../Resources/Images/ADC for Multi Tenants - New Page.png" MadCap:mediastyle="@media print { max-width: 5in; }" style="max-width: 5in;" />
        </p>
        <p>VERA adds the child tenant's name for the Connector to the beginning of the FQDN for the AD Connector. This naming convention results in the following requirements:</p>
        <ul>
            <li class="bullet">a wildcard certificate for each unique domain</li>
            <li class="bullet">additional DNS entries to tie the aliases to the actual Connector cluster</li>
        </ul>
        <p>In the above example, the DNS name for the global Connector already exists: adconnector.domain.com. VERA automatically creates a host name of child1-adconnector.domain.com. A DNS CN&#160;entry must be manually added for child1-adconnector.domain.com to point to adconnector.domain.com. </p>
        <h2>Summary of Steps</h2>
        <p>The following steps assume you have already created master and child tenants.</p>
        <ol>
            <li>Deploy the AD Connector for the master tenant.</li>
            <li>In your DNS, register a new CNAME as an instance of the AD Connector with the following naming convention: </li>
        </ol>
        <p class="listcont">&lt;tenant_name&gt;-&lt;Connector_FQDN&gt;</p>
        <p class="listcont">See "<a href="#Auto-gen">Auto-generated Aliases</a>" for an example.</p>
        <ol MadCap:continue="true">
            <li>Log into the VERA portal for the AD Connector.</li>
        </ol>
        <p class="listcont">
            <img src="../../Resources/Images/adc-pic2.png" MadCap:mediastyle="@media print { max-width: 6in; }" style="max-width: 6in;" />
        </p>
        <ol MadCap:continue="true">
            <li>Go to the <span class="UI">Instances</span> page.</li>
            <li>Select <span class="UI">Create Connector Instance</span> from the action menu.</li>
            <li>Enter the name of the child tenant to which you want to associate the instance. </li>
            <li>Enter the email address for the instance administrator.</li>
            <li>Click <span class="UI">Create</span>.</li>
        </ol>
        <p class="listcont">An invitation is sent to the instance administrator using the email address you provided. </p>
        <ol MadCap:continue="true">
            <li>Open the invitation and complete the on-boarding steps:</li>
        </ol>
        <ol style="list-style-type: lower-alpha;">
            <li class="level2">Enter the email address.</li>
            <li class="level2">Enter the password.</li>
            <li class="level2">Click <span class="UI">Next</span>.</li>
        </ol>
        <p class="listcont">
            <img src="../../Resources/Images/enableservicesv2.png" MadCap:mediastyle="@media print { max-width: 4in; }" style="width: 658px;height: 273px;" />
        </p>
        <ol start="10">
            <li>Select <span class="UI">AD Connector</span>.</li>
            <li>Click on the <span class="UI">Get Keys</span> button.</li>
        </ol>
        <p class="listcont">Note: This step should launch the <MadCap:glossaryTerm glossTerm="NewGlossary.Term5">Admin Portal</MadCap:glossaryTerm> for your VERA tenant. If it doesn’t launch, enter the tenant URL in a separate browser window.</p>
        <ol MadCap:continue="true">
            <li>Log into your VERA admin portal with administrator credentials.</li>
            <li>In the cloud VERA admin portal, select <span class="UI">Connectors</span>.</li>
        </ol>
        <p class="listcont">
            <img src="../../Resources/Images/Connectors/ConnectorAfterKey.png" style="width: 469px;height: 321px;">
            </img>
        </p>
        <ol MadCap:continue="true">
            <li>Click on the “gear” icon at top right of screen.</li>
        </ol>
        <p class="listcont">
            <img src="../../Resources/Images/Connectors/ConnectorKeyList2.png" style="width: 746px;height: 150px;" MadCap:mediastyle="@media print { max-width: 5in; }">
            </img>
        </p>
        <ol MadCap:continue="true">
            <li>Select the <span class="UI">AD Connector Key</span> option.</li>
            <li>Copy the displayed AD Connector key.</li>
            <li>Return to the AD Connector browser screen.</li>
            <li>Paste in the key.</li>
            <li>Click <span class="UI">Next</span>.</li>
        </ol>
        <p class="listcont">
            <img src="../../Resources/Images/Connectors/ADConfig90.png" MadCap:mediastyle="@media print { max-width: 5in; }" style="width: 542px;height: 417px;">
            </img>
        </p>
        <ol MadCap:continue="true">
            <li>Enable use of the VERA AD Connector:</li>
        </ol>
        <ol style="list-style-type: lower-alpha;">
            <li class="level2">AD Server Hostname: Enter the hostname of the server on which your Active Directory system is installed.</li>
            <li class="level2">Port: Enter the port on which AD is listening.</li>
            <li class="level2">Enable SSL: Select if the AD server requires secure SSL connections (LDAPS).</li>
            <li class="level2">Require LDAPS certificate to be signed by a trusted Certificate Authority: Select to disallow self-signed certificates.</li>
            <li class="level2">Enable Distribution Group Support: Select to cause VERA to assess an email address for use as a distribution list. If the email address does identify a list of other email addresses, VERA creates a membership list associated with the distribution group. Only known VERA users are added to the membership list. </li>
            <li class="level2">Base DN: Enter the location to search for users and groups in Active Directory. Use LDAP format. For example, if the domain components are mydomain and com, you would enter <span class="entry">dc=mydomain, dc=com</span>.</li>
            <li class="level2">User Name: Enter a service account having Read privileges for your Active Directory system. </li>
            <li class="level2">Password: Enter the password for the user name you just entered.</li>
            <li class="level2">Advanced Settings: Click to specify separate base DNs for users and groups if you have configured your underlying directory to require separation. Leaving either or both of the fields blank causes VERA to use the Base DN.</li>
            <li class="level2">Click <span class="UI">Complete</span>.</li>
        </ol>
        <ol start="21">
            <li>Confirm the configuration:</li>
        </ol>
        <ol style="list-style-type: lower-alpha;">
            <li class="level2">Make sure the Connector you just configured displays in the <span class="UI">Server Farm</span> page under <span class="UI">Nodes</span>.</li>
        </ol>
        <p class="listcont">
            <img src="../../Resources/Images/Connectors/ConnectorConfirm.png" alt="" title="" style="width: 497px;height: 349px;" MadCap:mediastyle="@media print { max-width: 4in; }">
            </img>
        </p>
        <ol style="list-style-type: lower-alpha;" MadCap:continue="true">
            <li class="level2">In your VERA tenant Admin Portal, make sure you see a node in the Connectors server farm.</li>
            <li class="level2">Confirm that the administrator has received an email with “ADC is UP” in the subject.</li>
        </ol>
    </body>
</html>