﻿<?xml version="1.0" encoding="utf-8"?>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd" xml:lang="en-us">
    <head><title></title>
        <link href="../../Resources/Stylesheets/SSH access to VERA connectors using the SSH Key in Terminal and PuTTY.css" rel="stylesheet" />
    </head>
    <body>
        <p class="labelKB" MadCap:conditions="Default.ScreenOnly">Knowledge Base</p>
        <h1>SSH Access to VERA Connectors <a href="../../Resources/Published/PDFs/SSHToConnectors.pdf" target="_blank"><img src="../../Resources/Images/MiscAssets/pdf.jpg" style="width: 40px;height: 42px;" MadCap:conditions="Default.ScreenOnly"></img></a></h1>
        <p>After installing a Connector, you may find the need to remotely SSH into the Connector to look at log files or perform other maintenance.&#160;You can do this using <span class="emphasis">ssh -i</span> from a terminal window or using other popular remote connection software, such as PuTTY.&#160; Either approach will require that you first acquire the SSH key from the Connector.</p>
        <h2>Where Is the SSH Key?</h2>
        <p>The SSH key that is needed to remotely access a Connector can be downloaded from the UI of the Connector:&#160; </p>
        <ol>
            <li>Log into your Connector with a browser (https://fqdn.domain.com).</li>
        </ol>
        <p class="listcont">If your DNS doesn't resolve the FQDN used to configure the Connector, try https://ipAddressOfConnector.</p>
        <p class="listcont">When you log in, the Appliance page displays.</p>
        <ol MadCap:continue="true">
            <li>Scroll down to the SSH key information at the bottom of the page.&#160; </li>
        </ol>
        <p class="listcont">
            <img src="../../Resources/Images/SSH access to VERA connectors using the SSH Key in Terminal and PuTTY/SSH access to VERA connectors.png" style="visibility: visible;mso-wrap-style: square;width: 402px;height: 163px;" />
        </p>
        <ol MadCap:continue="true">
            <li>If you see key info in the box, use the <span class="UI">Download Key</span> button to save a copy of the SSH key to your host machine.</li>
        </ol>
        <ol start="4">
            <li>If the box in the SSH Key section is blank:</li>
        </ol>
        <ol style="list-style-type: lower-alpha;">
            <li class="level2">Use the <span class="UI">Generate Key</span> button to create an SSH Key</li>
            <li class="level2">Download it to your machine.&#160; </li>
        </ol>
        <ol start="5">
            <li>Note where you save the key (id_rsa), and consider renaming the file to help identify which Connector or cluster of Connectors it is associated with.&#160; </li>
        </ol>
        <p class="listcont"><span class="emphasis">Note:</span> If you are connecting to one Connector within a cluster, the SSH key is shared amongst the nodes of a cluster.&#160; This means that you only need to download one key per cluster of Connectors.</p>
        <h2>Preparing the SSH Key for Use in Terminal or with PuTTY</h2>
        <ol>
            <li>Confirm with your network administrator that you have access to the Connector from your host machine on port 22.</li>
            <li>Open a terminal window.</li>
            <li>Change the working directory to match the location in which you downloaded the SSH key.&#160; </li>
            <li>Confirm access by entering:</li>
        </ol>
        <p class="listcont"><span class="entry">telnet &lt;FQDN&gt; 22</span>
        </p>
        <p class="listcont">Example:</p>
        <p class="listcont">
            <img src="../../Resources/Images/SSH access to VERA connectors using the SSH Key in Terminal and PuTTY/SSH access to VERA connectors_1.png" style="visibility: visible;mso-wrap-style: square;width: 421px;height: 72px;" />
        </p>
        <ol MadCap:continue="true">
            <li>Once we have confirmed you should be able to reach the Connector, enter the following command to change the permissions of the key to be read by the owner:</li>
        </ol>
        <p class="listcont"><span class="entry">chmod 400 &lt;keyfilename&gt;</span>
        </p>
        <p class="listcont">Example:</p>
        <p class="listcont">
            <img src="../../Resources/Images/SSH access to VERA connectors using the SSH Key in Terminal and PuTTY/SSH access to VERA connectors_2.png" style="visibility: visible;mso-wrap-style: square;width: 438px;height: 37px;" />
        </p>
        <h2>Remotely Connecting from a Terminal Window (Mac)</h2>
        <p>Now that you have the key downloaded, permissions to read the key, and an open path to the Connector, it is time to run the ssh command:</p>
        <p><span class="entry">$ ssh -i PathToSSHkey user@&lt;connectorIP&gt; </span>
        </p>
        <p><span class="emphasis">Note:</span> It is more accurate to use the IP address of the Connector that you want to reach since the FQDN of the Connector could be shared with an entire cluster, and the load balancer may connect you to any of the Connectors in that cluster.</p>
        <p>Example:</p>
        <p>
            <img src="../../Resources/Images/SSH access to VERA connectors using the SSH Key in Terminal and PuTTY/SSH access to VERA connectors_3.png" style="visibility: visible;mso-wrap-style: square;width: 509px;height: 405px;" />
        </p>
        <h2>Remotely Connecting from PuTTY (Windows)</h2>
        <p>Now that you have the key downloaded, permissions to read the key, and an open path to the Connector, it is time to run the ssh command:</p>
        <ol>
            <li>If you don't already have PuTTY installed on your Windows host machine, download the installer from putty.org and run the installer.</li>
            <li>Use PuTTYgen (included with the PuTTY installation) to convert the downloaded key to a format that is compatible with PuTTY:</li>
        </ol>
        <ol style="list-style-type: lower-alpha;">
            <li class="level2">Open PuTTYgen on your machine. </li>
            <li class="level2">In the Actions section, find the line that reads “Load an existing private key file”.&#160; </li>
            <li class="level2">Click the button to load a key file.&#160; </li>
            <li class="level2">Change the file type that you are looking for from .ppk to All Files.&#160; </li>
            <li class="level2">Navigate to the saved SSH key file.</li>
            <li class="level2">Select the file to load.&#160; </li>
            <li class="level2">Click OK.</li>
        </ol>
        <p class="listcont">
            <img src="../../Resources/Images/SSH access to VERA connectors using the SSH Key in Terminal and PuTTY/SSH access to VERA connectors_4.png" style="visibility: visible;mso-wrap-style: square;width: 365px;height: 296px;" />
        </p>
        <ol style="list-style-type: lower-alpha;" MadCap:continue="true">
            <li class="level2">Click Save private key.</li>
        </ol>
        <p class="listcont">
            <img src="../../Resources/Images/SSH access to VERA connectors using the SSH Key in Terminal and PuTTY/SSH access to VERA connectors_5.png" style="visibility: visible;mso-wrap-style: square;width: 366px;height: 301px;" />
        </p>
        <ol style="list-style-type: lower-alpha;" MadCap:continue="true">
            <li class="level2">Click Yes.</li>
            <li class="level2">Choose a name to save the new .ppk file and note where you are saving the file.&#160; </li>
        </ol>
        <ol start="3">
            <li>Open PuTTY and fill in the info for the Hostname or IP for the machine to which you are connecting.</li>
        </ol>
        <p class="listcont">
            <img src="../../Resources/Images/SSH access to VERA connectors using the SSH Key in Terminal and PuTTY/SSH access to VERA connectors_6.png" style="visibility: visible;mso-wrap-style: square;width: 306px;height: 295px;" />
        </p>
        <ol MadCap:continue="true">
            <li>Click to expand the menu for SSH under Connection on the left hand tree-structure.&#160; </li>
            <li>Click on the Auth option and find the box in the right hand pane under “Private key file for authentication”.</li>
        </ol>
        <p class="listcont">
            <img src="../../Resources/Images/SSH access to VERA connectors using the SSH Key in Terminal and PuTTY/SSH access to VERA connectors_7.png" style="visibility: visible;mso-wrap-style: square;width: 318px;height: 304px;" />
        </p>
        <ol MadCap:continue="true">
            <li>Click the Browse button and find the keyfile.ppk that you saved earlier.</li>
        </ol>
        <p class="listcont">
            <img src="../../Resources/Images/SSH access to VERA connectors using the SSH Key in Terminal and PuTTY/SSH access to VERA connectors_8.png" style="visibility: visible;mso-wrap-style: square;width: 338px;height: 226px;" />
        </p>
        <ol MadCap:continue="true">
            <li>Click Open to load the path to that key file into PuTTY.</li>
            <li>Click Open again to initiate the SSH connection to the Connector.</li>
        </ol>
        <p class="listcont">If this is the first time connecting (in this method or from this machine) to the VERA Connector, you will be asked to trust the SSH Key.&#160; </p>
        <p class="listcont">
            <img src="../../Resources/Images/SSH access to VERA connectors using the SSH Key in Terminal and PuTTY/SSH access to VERA connectors_9.png" style="visibility: visible;mso-wrap-style: square;width: 394px;height: 229px;" />
        </p>
        <ol MadCap:continue="true">
            <li>Click “Yes” to continue.</li>
        </ol>
        <p class="listcont">You will see the following prompt:</p>
        <p class="listcont">
            <img src="../../Resources/Images/SSH access to VERA connectors using the SSH Key in Terminal and PuTTY/SSH access to VERA connectors_10.png" style="visibility: visible;mso-wrap-style: square;width: 401px;height: 125px;" />
        </p>
        <ol MadCap:continue="true">
            <li>Type <span class="entry">vera </span>and press Enter. </li>
        </ol>
        <p class="listcont">
            <img src="../../Resources/Images/SSH access to VERA connectors using the SSH Key in Terminal and PuTTY/SSH access to VERA connectors_11.png" style="visibility: visible;mso-wrap-style: square;width: 414px;height: 262px;" />
        </p>
        <p>&#160;</p>
        <p>&#160;</p>
    </body>
</html>