﻿<?xml version="1.0" encoding="utf-8"?>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd" MadCap:lastBlockDepth="5" MadCap:lastHeight="388" MadCap:lastWidth="902">
    <head>
        <link href="../../TableStyles/KBTable1.css" rel="stylesheet" MadCap:stylesheetType="table" />
    </head>
    <body>
        <p>For <span class="emphasis">internal authentication</span>, most VERA customers consider the following options:</p>
        <ul>
            <li class="bullet">AD FS</li>
            <li class="bullet">SAML</li>
            <li class="bullet">VERA <MadCap:glossaryTerm glossTerm="NewGlossary.Term18">AD&#160;Connector</MadCap:glossaryTerm></li>
        </ul>
        <p>The following considerations are key during this evaluation:</p>
        <ul>
            <li class="bullet">basic user authentication</li>
            <li class="bullet">security groups</li>
            <li class="bullet">distribution lists</li>
        </ul>
        <p>The following table summarizes the support for each area of interest:</p>
        <table style="width: 60%;mc-table-style: url('../../TableStyles/KBTable1.css');" class="TableStyle-KBTable1" cellspacing="0">
            <col class="TableStyle-KBTable1-Column-Regular" style="width: 121px;">
            </col>
            <col class="TableStyle-KBTable1-Column-Regular" style="width: 81px;">
            </col>
            <col class="TableStyle-KBTable1-Column-Regular" style="width: 81px;">
            </col>
            <col class="TableStyle-KBTable1-Column-Regular" style="width: 109px;">
            </col>
            <tbody>
                <tr class="TableStyle-KBTable1-Body-LightRows">
                    <th class="TableStyle-KBTable1-BodyE-Regular-LightRows">&#160;</th>
                    <th class="TableStyle-KBTable1-BodyE-Regular-LightRows">AD FS</th>
                    <th class="TableStyle-KBTable1-BodyE-Regular-LightRows">SAML</th>
                    <th class="TableStyle-KBTable1-BodyD-Regular-LightRows">AD&#160;Connector</th>
                </tr>
                <tr class="TableStyle-KBTable1-Body-DarkerRows">
                    <td class="TableStyle-KBTable1-BodyE-Regular-DarkerRows">User Authentication</td>
                    <td class="TableStyle-KBTable1-BodyE-Regular-DarkerRows">yes</td>
                    <td class="TableStyle-KBTable1-BodyE-Regular-DarkerRows">yes</td>
                    <td class="TableStyle-KBTable1-BodyD-Regular-DarkerRows">yes</td>
                </tr>
                <tr class="TableStyle-KBTable1-Body-LightRows">
                    <td class="TableStyle-KBTable1-BodyE-Regular-LightRows">Security Groups</td>
                    <td class="TableStyle-KBTable1-BodyE-Regular-LightRows">if exposed</td>
                    <td class="TableStyle-KBTable1-BodyE-Regular-LightRows">if exposed</td>
                    <td class="TableStyle-KBTable1-BodyD-Regular-LightRows">yes</td>
                </tr>
                <tr class="TableStyle-KBTable1-Body-DarkerRows">
                    <td class="TableStyle-KBTable1-BodyB-Regular-DarkerRows">Distribution Lists</td>
                    <td class="TableStyle-KBTable1-BodyB-Regular-DarkerRows">no</td>
                    <td class="TableStyle-KBTable1-BodyB-Regular-DarkerRows">no</td>
                    <td class="TableStyle-KBTable1-BodyA-Regular-DarkerRows">yes</td>
                </tr>
            </tbody>
        </table>
    </body>
</html>