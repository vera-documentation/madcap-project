﻿<?xml version="1.0" encoding="utf-8"?>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd">
    <head>
        <link href="../../TableStyles/KBTable1.css" rel="stylesheet" MadCap:stylesheetType="table" />
    </head>
    <body>
        <ol>
            <li>Configure your VERA tenant with your SAML identity provider.</li>
        </ol>
        <p class="listcont">See one of the following sections for an example:</p>
        <ul>
            <li>SAML Setup: PingFederate<![CDATA[  ]]></li>
            <li>SAML Setup: AD FS </li>
            <li>SAML Setup: Okta</li>
        </ul>
        <p class="listcont">Important: Be sure to get the SAML metadata from the SAML&#160;identity provider when you configure your tenant.</p>
        <ol start="2">
            <li>In the VERA <MadCap:glossaryTerm glossTerm="NewGlossary.Term5">Admin Portal</MadCap:glossaryTerm>, go to <span class="UI">Settings &gt; Authentication</span>.</li>
            <li>Click the <span class="UI">Configurations</span> link at the top of the screen.</li>
            <li>Click <span class="UI">New Configuration</span>.</li>
            <li>Set <span class="UI">Authentication Type</span> to <span class="UI">SAML</span>. </li>
        </ol>
        <ol MadCap:continue="true">
            <li>For <span class="UI">Config Name</span>, enter text that defines the configuration and its purpose.</li>
        </ol>
        <ol MadCap:continue="true">
            <li>Click <span class="UI">Choose a file</span> to upload the metadata file.</li>
        </ol>
        <p class="listcont">Make sure the file ends in .xml.</p>
        <ol MadCap:continue="true">
            <li>Click <span class="UI">Save</span>.</li>
            <li>Use the following table as a guideline for completing the remaining fields:</li>
        </ol>
        <MadCap:snippetBlock src="SAMLSettings.flsnp" />
        <ol MadCap:continue="true">
            <li>If you want to manually create the corresponding SAML&#160;groups: </li>
        </ol>
        <ol style="list-style-type: lower-alpha;">
            <li class="level2">Make sure you did not set a Groups Attribute value.</li>
            <li class="level2">Clear the <span class="UI">Auto-Create SAML Groups as Local Groups</span> option.</li>
            <li class="level2">Return to the Home page in the Admin Portal. </li>
            <li class="level2">Select <span class="UI">Groups</span>. </li>
            <li class="level2">Select the action menu.</li>
            <li class="level2">Select <span class="UI">Create Group</span>.</li>
            <li class="level2">Create a group matching one of your SAML groups.</li>
            <li class="level2">Repeat steps d through f for each SAML group.</li>
        </ol>
        <ol start="12">
            <li>Click <span class="UI">Save</span>.</li>
        </ol>
        <ol MadCap:continue="true">
            <li>Click <span class="UI">Rules</span>.</li>
            <li> Expand the SAML entry to set the domains, groups, and/or users you want to authenticate using SAML.</li>
        </ol>
        <p class="listcont">For domains, enter the domain (or domains) that your internal users will be specifying in their email address.  For example, if a user logs in with user@acme.com, then specify acme.com.</p>
        <p class="listcont"><span class="emphasis">Note:</span> If an internal domain or user is not specified, then no user will be directed to the SAML&#160;provider to authenticate. We recommend specifying a secondary user instead of a domain for initial testing to avoid possibly locking yourself out of the Admin Portal if SAML is not initially configured correctly. See this Best Practices article for information.</p>
    </body>
</html>