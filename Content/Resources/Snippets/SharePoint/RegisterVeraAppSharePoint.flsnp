﻿<?xml version="1.0" encoding="utf-8"?>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd">
    <body>
        <p>To integrate VERA with SharePoint, you need to configure a special VERA app for SharePoint. This app provides the necessary access to the SharePoint collection containing the files you want to auto-secure with VERA. </p>
        <p>The first step in configuring this app is registering it with your SharePoint tenant. To complete this step, you will need:</p>
        <ul>
            <li class="bullet">the credentials for the SharePoint <span class="emphasis">global admin account</span></li>
            <li class="bullet">the URL for your VERA tenant</li>
        </ul>
        <ol>
            <li>In SharePoint, go to App Registration page:</li>
        </ol>
        <p class="listcont">
            <MadCap:conditionalText>https://&lt;mysharepoint&gt;.sharepoint.com/_layouts/15/appregnew.aspx</MadCap:conditionalText>
            <MadCap:conditionalText>https://&lt;mySharePointServerURL&gt;_layouts/15/appregnew.aspx</MadCap:conditionalText>
        </p>
        <ol MadCap:continue="true">
            <li>Log in with the credentials for the global admin account.</li>
        </ol>
        <p class="listcont">
            <img src="../../Images/SharePoint/SPAdminLogin.png" style="width: 597px;height: 320px;" MadCap:mediastyle="@media print { max-width: 6in; }" />
            <img src="../../Images/SPOnPremAppID.png" MadCap:mediastyle="@media print { max-width: 6in; }" style="width: 599px;height: 308px;" />
        </p>
        <ol MadCap:continue="true">
            <li>Click <span class="UI">Generate</span> next to <span class="UI">Client Id</span>.</li>
        </ol>
        <ol MadCap:continue="true">
            <li>Click <span class="UI">Generate</span> next to <span class="UI">App Id</span>.</li>
        </ol>
        <ol MadCap:continue="true">
            <li>Click <span class="UI">Generate</span> next to <span class="UI">Client Secret</span>.</li>
            <li>In the <span class="UI">App Title</span> field, assign a title to use for referring to the app.</li>
        </ol>
        <p class="listcont"><span class="emphasis">Be sure to use a unique and descriptive title so that you can differentiate this app from the others in your deployment. </span>
        </p>
        <ol MadCap:continue="true">
            <li>In the <span class="UI">App Domain</span> field, enter the URL for your VERA tenant (e.g., mytenant.vera.com).</li>
            <li>In the <span class="UI">Redirect URL</span> field, enter the following path:</li>
        </ol>
        <p class="listcont">https://&lt;mytenantURL&gt;/api/fs/config/account/sharepoint/verify</p>
        <p class="listcont">Example: https://acme.vera.com/api/fs/config/account/sharepoint/verify</p>
        <ol MadCap:continue="true">
            <li>Click <span class="UI">Create</span>.</li>
        </ol>
        <p class="listcont">
            <img src="../../Images/SharePoint/SPAppInfo.png" style="width: 389px;height: 202px;" />
        </p>
        <ol MadCap:continue="true">
            <li><span class="emphasis">Save this information!</span>
            </li>
        </ol>
        <p class="listcont"><span class="emphasis">Don't just take a screenshot.</span> Copy and paste the information in a text file. You will need to provide it when you create the VERA Share.</p>
    </body>
</html>