﻿<?xml version="1.0" encoding="utf-8"?>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd">
    <head>
        <link href="../../TableStyles/KBTable1.css" rel="stylesheet" MadCap:stylesheetType="table" />
    </head>
    <body>
        <p>The folder paths you will need for VERA Rules are based on the SharePoint-specific elements in your deployment. These elements include things like site collections, libraries, collections, and folders as defined in SharePoint, but VERA sees them all as folders and subfolders. The SharePoint Online rule must point to a full, not a partial, folder name. Use the following table as a guide when determining the paths that will meet your needs. Your paths must include your SharePoint domain, but omit the "https://" prefix.</p>
        <table style="width: 100%;mc-table-style: url('../../TableStyles/KBTable1.css');" class="TableStyle-KBTable1" cellspacing="0">
            <col class="TableStyle-KBTable1-Column-Regular" />
            <col class="TableStyle-KBTable1-Column-Regular" />
            <tbody>
                <tr class="TableStyle-KBTable1-Body-LightRows">
                    <th class="TableStyle-KBTable1-BodyE-Regular-LightRows">Target</th>
                    <th class="TableStyle-KBTable1-BodyD-Regular-LightRows">Folder Path</th>
                </tr>
                <tr class="TableStyle-KBTable1-Body-DarkerRows">
                    <td class="TableStyle-KBTable1-BodyE-Regular-DarkerRows">All files and folders under MySiteCollection</td>
                    <td class="TableStyle-KBTable1-BodyD-Regular-DarkerRows">
                        <MadCap:conditionalText>sharepoint.mydomain.com</MadCap:conditionalText>
                        <MadCap:conditionalText>mydomain.sharepoint.com</MadCap:conditionalText>/sites/MySiteCollection</td>
                </tr>
                <tr class="TableStyle-KBTable1-Body-LightRows">
                    <td class="TableStyle-KBTable1-BodyE-Regular-LightRows">All files and folders under default library</td>
                    <td class="TableStyle-KBTable1-BodyD-Regular-LightRows">
                        <MadCap:conditionalText>sharepoint.mydomain.com</MadCap:conditionalText>
                        <MadCap:conditionalText>mydomain.sharepoint.com</MadCap:conditionalText>/sites/MySiteCollectoin/Shared Documents</td>
                </tr>
                <tr class="TableStyle-KBTable1-Body-DarkerRows">
                    <td class="TableStyle-KBTable1-BodyE-Regular-DarkerRows">All files and folders under MyFolder in the default library</td>
                    <td class="TableStyle-KBTable1-BodyD-Regular-DarkerRows">
                        <MadCap:conditionalText>sharepoint.mydomain.com</MadCap:conditionalText>
                        <MadCap:conditionalText>mydomain.sharepoint.com</MadCap:conditionalText>/sites/MySiteCollection/Shared Documents/MyFolder</td>
                </tr>
                <tr class="TableStyle-KBTable1-Body-LightRows">
                    <td class="TableStyle-KBTable1-BodyE-Regular-LightRows">All files and folders under MyLibrary in MySiteCollection</td>
                    <td class="TableStyle-KBTable1-BodyD-Regular-LightRows">
                        <MadCap:conditionalText>sharepoint.mydomain.com</MadCap:conditionalText>
                        <MadCap:conditionalText>mydomain.sharepoint.com</MadCap:conditionalText>/sites/MySiteCollection/MyLibrary</td>
                </tr>
                <tr class="TableStyle-KBTable1-Body-DarkerRows">
                    <td class="TableStyle-KBTable1-BodyB-Regular-DarkerRows">All libraries, files, and folders under MySubsite in MySiteCollection</td>
                    <td class="TableStyle-KBTable1-BodyA-Regular-DarkerRows">
                        <MadCap:conditionalText>sharepoint.mydomain.com</MadCap:conditionalText>
                        <MadCap:conditionalText>mydomain.sharepoint.com</MadCap:conditionalText>/sites/MySiteCollection/MySubSite</td>
                </tr>
            </tbody>
        </table>
    </body>
</html>