﻿<?xml version="1.0" encoding="utf-8"?>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd">
    <body>
        <MadCap:snippetBlock src="IntegrationBoilerplate.flsnp" />
        <h2>Without VERA</h2>
        <p>Consider a basic enterprise deployment for <MadCap:variable name="MyVariables.ProductA" /> without a VERA integration.</p>
        <p>
            <img src="../../Images/Integrations/BoxDropboxConcept.png" MadCap:mediastyle="@media print { max-width: 3.5in; }" />
        </p>
        <p>In this case, a user drags a file into the designated folder, where the access and permissions defined in <MadCap:variable name="MyVariables.ProductA" /> are applied and the files are synced to provide access to authorized collaborators. However, if an authorized collaborator emails the file to a colleague, those access rules and permissions no longer apply. </p>
        <h2>How the VERA Integration Works</h2>
        <p>VERA uses two features to establish its <MadCap:variable name="MyVariables.ProductA" /> integration:</p>
        <ul>
            <li class="bullet">Rules determine which files get auto-secured.</li>
            <li class="bullet">Shares determine who can access the auto-secured files and with what restrictions.</li>
        </ul>
        <h3>Rules Define What Gets Secured</h3>
        <p>When we integrate VERA with <MadCap:variable name="MyVariables.ProductA" />, we start by creating a rule that defines the folder whose contents will be auto-secured. </p>
        <p>
            <img src="../../Images/Integrations/BoxDropboxRuleConcept.png" style="width: 457px;height: 298px;" MadCap:mediastyle="@media print { max-width: 3.5in; }" />
        </p>
        <p>When a user places a file in the designated folder, VERA encrypts the file and applies a wrapper that provides VERA's added control.</p>
        <h3>Shares Define Who Can Access Secure Files </h3>
        <MadCap:snippetBlock src="../Box/ShareConceptBox.flsnp" />
        <h2>Securing Files Added from Web/Mobile Interfaces</h2>
        <p>If your users are likely to use the <MadCap:variable name="MyVariables.ProductA" /> web interface or mobile app to add files to the folder, then they can bypass your rule and introduce files that will not be auto-secured. </p>
        <p>
            <img src="../../Images/Integrations/BoxDropboxNoConnector.png" MadCap:mediastyle="@media print { max-width: 3.5in; }" />
        </p>
        <p>To make sure these files are secured, you can add a Share <MadCap:glossaryTerm glossTerm="NewGlossary.Term3">Connector</MadCap:glossaryTerm>. The Share Connector watches the folder in the cloud and ensures that all files are appropriately secured.</p>
        <p>Note: The Share Connector deployment discussed in this guide is an on-premise deployment. VERA-hosted cloud deployments are available, as well. Contact VERA for information on these deployments.</p>
        <p>
            <img src="../../Images/Integrations/BoxDropboxwConnector.png" MadCap:mediastyle="@media print { max-width: 3.5in; }" />
        </p>
        <h2>Deploying the Integration: What You Need</h2>
        <p>A <MadCap:variable name="MyVariables.ProductA" /> integration generally has the following components:</p>
        <ul>
            <li class="bullet">VERA rule</li>
            <li class="bullet">VERA share</li>
            <li class="bullet">
                <MadCap:glossaryTerm glossTerm="NewGlossary.Term4">VERA Client</MadCap:glossaryTerm>
            </li>
            <li class="bullet">VERA Share Connector</li>
            <li class="bullet">a business account for <MadCap:variable name="MyVariables.ProductA" /> </li>
        </ul>
        <p class="listcontl2"><span class="emphasis">Note:</span> You will use this account to establish VERA access to your organization's <MadCap:variable name="MyVariables.ProductA" /> account. You can undo this access at any time from the <MadCap:variable name="MyVariables.ProductA" /> account. </p>
    </body>
</html>