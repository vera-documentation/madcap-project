﻿<?xml version="1.0" encoding="utf-8"?>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd" MadCap:lastBlockDepth="6" MadCap:lastHeight="1907" MadCap:lastWidth="860">
    <head>
        <link href="../../TableStyles/KBTable1.css" rel="stylesheet" MadCap:stylesheetType="table" />
    </head>
    <body>
        <ol>
            <li>Go to <span class="UI">Settings &gt; Rules</span>.</li>
        </ol>
        <p class="listcont">
            <img src="../../Images/Integrations/RulesGetStarted.png" style="width: 638px;height: 318px;" />
        </p>
        <ol MadCap:continue="true">
            <li>Click <span class="UI">Get Started</span>. </li>
        </ol>
        <p class="listcont">If this is not your first rule, Get Started won't be displayed. Click Create Rule, instead. </p>
        <p class="listcont">
            <img src="../../Images/Integrations/CreateRule.png" style="width: 375px;height: 179px;" />
        </p>
        <ol MadCap:continue="true">
            <li>Select the type of rule (OneDrive).</li>
            <li>Assign a name to the rule.</li>
            <li>Click <span class="UI">Create</span>.</li>
        </ol>
        <p class="listcont">
            <img src="../../Images/Integrations/OneDriveRuleBlank.png" MadCap:mediastyle="@media print { max-width: 4in; }" style="width: 617px;height: 386px;" />
        </p>
        <ol MadCap:continue="true">
            <li>Build your filter rule:</li>
        </ol>
        <table style="mc-table-style: url('../../TableStyles/KBTable1.css');width: 80%;" class="TableStyle-KBTable1" cellspacing="0">
            <col class="TableStyle-KBTable1-Column-Regular" style="width: 116px;" />
            <col class="TableStyle-KBTable1-Column-Regular" style="width: 114px;" />
            <col class="TableStyle-KBTable1-Column-Regular" />
            <col class="TableStyle-KBTable1-Column-Regular" />
            <col class="TableStyle-KBTable1-Column-Regular" />
            <tbody>
                <tr class="TableStyle-KBTable1-Body-LightRows">
                    <th class="TableStyle-KBTable1-BodyE-Regular-LightRows">Item</th>
                    <th class="TableStyle-KBTable1-BodyE-Regular-LightRows">Operator</th>
                    <th class="TableStyle-KBTable1-BodyE-Regular-LightRows">Operator</th>
                    <th class="TableStyle-KBTable1-BodyE-Regular-LightRows">Item</th>
                    <th class="TableStyle-KBTable1-BodyD-Regular-LightRows">Item</th>
                </tr>
                <tr class="TableStyle-KBTable1-Body-DarkerRows">
                    <td class="TableStyle-KBTable1-BodyE-Regular-DarkerRows">Folder Path</td>
                    <td rowspan="4" class="TableStyle-KBTable1-BodyE-Regular-DarkerRows">Does/Does Not</td>
                    <td class="TableStyle-KBTable1-BodyE-Regular-DarkerRows">Starts With / Ends With / Contains</td>
                    <td class="TableStyle-KBTable1-BodyE-Regular-DarkerRows">Documents / Pictures / Music / Videos / Desktop</td>
                    <td class="TableStyle-KBTable1-BodyD-Regular-DarkerRows">
                        <p>Sub folders (Required)</p>
                        <p><span class="emphasis">Note: Do not use / to start this entry.</span>
                        </p>
                    </td>
                </tr>
                <tr class="TableStyle-KBTable1-Body-LightRows">
                    <td class="TableStyle-KBTable1-BodyE-Regular-LightRows">Sub Folder</td>
                    <td class="TableStyle-KBTable1-BodyE-Regular-LightRows">Starts With / Ends With / Contains</td>
                    <td class="TableStyle-KBTable1-BodyE-Regular-LightRows">&#160;</td>
                    <td class="TableStyle-KBTable1-BodyD-Regular-LightRows">&#160;</td>
                </tr>
                <tr class="TableStyle-KBTable1-Body-DarkerRows">
                    <td class="TableStyle-KBTable1-BodyE-Regular-DarkerRows">Secure By</td>
                    <td class="TableStyle-KBTable1-BodyE-Regular-DarkerRows">Starts With / Ends With / Contains / Equals / Member Of</td>
                    <td class="TableStyle-KBTable1-BodyE-Regular-DarkerRows">user name</td>
                    <td class="TableStyle-KBTable1-BodyD-Regular-DarkerRows">n/a</td>
                </tr>
                <tr class="TableStyle-KBTable1-Body-LightRows">
                    <td class="TableStyle-KBTable1-BodyE-Regular-LightRows">File Name</td>
                    <td class="TableStyle-KBTable1-BodyE-Regular-LightRows">Starts With / Ends With / Contains / Equals</td>
                    <td class="TableStyle-KBTable1-BodyE-Regular-LightRows">file name</td>
                    <td class="TableStyle-KBTable1-BodyD-Regular-LightRows">n/a</td>
                </tr>
                <tr class="TableStyle-KBTable1-Body-DarkerRows" MadCap:conditions="">
                    <td class="TableStyle-KBTable1-BodyE-Regular-DarkerRows">Created Date</td>
                    <td class="TableStyle-KBTable1-BodyE-Regular-DarkerRows" rowspan="2" colspan="2">Older Than / Newer Than</td>
                    <td class="TableStyle-KBTable1-BodyD-Regular-DarkerRows" colspan="2" rowspan="2">Date</td>
                </tr>
                <tr class="TableStyle-KBTable1-Body-LightRows" MadCap:conditions="">
                    <td class="TableStyle-KBTable1-BodyB-Regular-LightRows">Modified Date</td>
                </tr>
            </tbody>
        </table>
        <ol MadCap:continue="true">
            <li>(Optional) For <span class="UI">Give access to</span>, select to define users with access and which policies to apply.</li>
            <li>(Optional) For <span class="UI">Allow users to share the file with others</span>, select to enable file sharing and which policy to apply.</li>
            <li>(Optional) For <span class="UI">Enforce rule even when manual securing is disabled</span>, select to apply the rule for users who do not have permission to secure files.</li>
            <li>Click&#160;<span class="UI">Save Rule</span>.</li>
            <li>If you haven't created a Share yet for OneDrive, you'll need to do that before your rule will work.</li>
        </ol>
    </body>
</html>