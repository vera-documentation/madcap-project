﻿<?xml version="1.0" encoding="utf-8"?>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd">
    <head>
        <link href="../../TableStyles/KBTable1.css" rel="stylesheet" MadCap:stylesheetType="table" />
        <link href="../../TableStyles/KBTable1.css" rel="stylesheet" MadCap:stylesheetType="table" />
    </head>
    <body>
        <h1>VERA Integration with SMB File Shares<MadCap:keyword term="SMB;Samba;File share;Internal;Externa;File share Connector"></MadCap:keyword></h1>
        <MadCap:snippetBlock src="SMBBenefits.flsnp" />
        <h2>Without VERA</h2>
        <MadCap:snippetBlock src="SMBNoRules.flsnp" />
        <h2>How the VERA Integration Works</h2>
        <MadCap:snippetBlock src="SMBComponents.flsnp" />
        <h3>Rules Define What Gets Secured</h3>
        <MadCap:snippetBlock src="SMBRules.flsnp" />
        <h3>A Share Connector Detects New Files and Secures Them</h3>
        <MadCap:snippetBlock src="SMBShareConnector.flsnp" />
        <h3>Shares Define Who Can Access Secure Files </h3>
        <MadCap:snippetBlock src="ShareDescriptionSMB.flsnp" />
        <h1>What a VERA Deployment Looks Like with SMB Integration</h1>
        <p>For a VERA integration with SMB, we generally recommend a deployment that includes:</p>
        <ul>
            <li class="bullet">a cluster of File Share Connectors</li>
            <li class="bullet">a cluster of AD Connectors</li>
        </ul>
        <p>
            <img src="../../Images/SMB File Share Connector with AD Connector.png" MadCap:mediastyle="@media print { max-width: 6in; }" style="max-width: 6in;" />
        </p>
        <h2>External Authentication Options</h2>
        <MadCap:snippetBlock src="../Auth/ExternalAuthOpts.flsnp" />
        <h1>FAQ: SMB&#160;Integration</h1>
        <p><span class="emphasis">Q: Why is an AD Connector recommended?</span>
        </p>
        <p>A: Since SMB means a local server with access provided to internal users, integrating with the existing Active Directory implementation provides direct access to existing users and groups. Integration with AD also allows email distribution groups to be recognized in the SMB integration.</p>
        <p><span class="emphasis">Q: Why is a cluster of Connectors recommended?</span>
        </p>
        <p>A: A cluster ensures functional redundancy in the event that a node becomes inoperable.  Each node can be placed in different geographical locations, but each must have access to all other nodes in the cluster.</p>
        <p><span class="emphasis">Q: What do I need to do to back up the clusters?</span>
        </p>
        <p>A: Share Connectors and AD Connectors are stateless appliances; therefore, backups are not needed. </p>
        <p><span class="emphasis">Q: How are permissions handled?</span>
        </p>
        <p>A: Existing VERA policies are mapped to NTFS permissions by the Share. Files that are then protected on that Share are assigned policies automatically.</p>
        <h1>Infrastructure Considerations and Checklists</h1>
        <h2>File Share Connector Prerequisites</h2>
        <MadCap:snippetBlock src="../Connectors/ConnectorsShareReqsRef.flsnp" />
        <h2>AD Connector Prerequisites</h2>
        <MadCap:snippetBlock src="../Auth/ADConnectorDeployReqs.flsnp" />
        <h2>Network Requirements</h2>
        <MadCap:snippetBlock src="../Connectors/ConnectorAllNetwork.flsnp" />
        <MadCap:snippetBlock src="ConnectorSMBNetwork.flsnp" />
        <MadCap:snippetBlock src="../ActiveDirectory/ConnectorADNetwork.flsnp" />
        <h2>File Share Connector Installation Checklist</h2>
        <p>You will need the following information for installing and configuring a File Share Connector:</p>
        <ul>
            <li class="bullet">IP</li>
            <li class="bullet">Netmask</li>
            <li class="bullet">Gateway</li>
            <li class="bullet">DNS</li>
            <li class="bullet">unique internal FQDN</li>
            <li class="bullet">hostname</li>
            <li class="bullet">proxy URL and credentials, if using a proxy</li>
            <li class="bullet">URL of your VERA tenant.</li>
            <li class="bullet">FQDN you want to assign to this VERA Share Connector.</li>
            <li class="bullet">DNS server IP or hostnames</li>
            <li class="bullet">SSL certificate (P12 or PFX) matching Connector FQDN</li>
        </ul>
        <h2>AD&#160;Connector Installation Checklist</h2>
        <MadCap:snippetBlock src="../ActiveDirectory/ConnectorsADReqsRef.flsnp" />
        <p>&#160;</p>
        <h1><a name="SMBSummary"></a>Summary of Steps</h1>
        <MadCap:snippetBlock src="SMBStepsSummary.flsnp" />
        <h2><a name="Important"></a>Important Tips</h2>
        <MadCap:snippetBlock src="SMBTips.flsnp" />
        <h1><a name="Creating"></a>Creating a VERA Rule for SMB</h1>
        <p>To set up an integration between VERA and SMB, you need to create a rule that defines the files to be auto-<MadCap:glossaryTerm glossTerm="NewGlossary.Term1">secured</MadCap:glossaryTerm>.</p>
        <h2>What You Will Need</h2>
        <p>Before you set up your rule, make sure you have:</p>
        <ul>
            <li class="bullet">the IP address of the file server on which the SMB share is located</li>
            <li class="bullet">the path to the folder you want to auto-secure</li>
        </ul>
        <MadCap:snippetBlock src="SMBRuleTask.flsnp" />
        <p>&#160;</p>
        <h1><a name="Installi"></a>Installing and Configuring a Share Connector</h1>
        <h2>Deploying the OVA&#160;Template</h2>
        <MadCap:snippetBlock src="../Connectors/ConnectorOVATask.flsnp" />
        <h2>Configuring the Base Connector</h2>
        <MadCap:snippetBlock src="../Connectors/ConnectorsBaseTask.flsnp" />
        <body>
            <h2>Configuring Share Connector Settings</h2>
        </body>
        <ol>
            <li>Select <span class="UI">Shares Connector</span>.</li>
            <li>Click on the <span class="UI">Get Keys</span> button.</li>
        </ol>
        <p class="listcont">Note: This step should launch the <MadCap:glossaryTerm glossTerm="NewGlossary.Term5">Admin Portal</MadCap:glossaryTerm> for your VERA tenant. If it doesn’t launch, enter the tenant URL in a separate browser window.</p>
        <ol MadCap:continue="true">
            <li>Log into your VERA admin portal with administrator credentials.</li>
            <li>In the cloud VERA admin portal, select <span class="UI">Connectors</span>.</li>
        </ol>
        <p>
            <img src="../../Images/Connectors/ConnectorAfterKey.png" MadCap:mediastyle="@media print { max-width: 5in; }" />
        </p>
        <ol MadCap:continue="true">
            <li>Click on the “gear” icon at top right of screen.</li>
        </ol>
        <p>
            <img src="../../Images/Connectors/ConnectorKeyList2.png" style="width: 801px;height: 162px;" MadCap:mediastyle="@media print { max-width: 5in; }" />
        </p>
        <ol MadCap:continue="true">
            <li>Select the <span class="UI">Shares Connector Key</span> option.</li>
            <li>Copy the displayed Shares Connector key.</li>
            <li>Return to the Shares Connector browser screen.</li>
            <li>Paste in the key.</li>
            <li>Click <span class="UI">Next</span>.</li>
            <li>Confirm the configuration:</li>
        </ol>
        <ol style="list-style-type: lower-alpha;">
            <li class="level2">Make sure the connector you just configured displays in the <span class="UI">Server Farm</span> page under <span class="UI">Nodes</span>.</li>
        </ol>
        <p><a><img src="../../Images/Connectors/ConnectorConfirm.png" alt="" title="" style="width: 518px;height: 360px;" /></a>
        </p>
        <ol style="list-style-type: lower-alpha;" MadCap:continue="true">
            <li class="level2">In your VERA tenant admin portal, make sure you see a node in the Connectors server farm.</li>
        </ol>
        <h2>High Availability</h2>
        <p>For production deployments of the VERA Share Connector, VERA recommends a minimum of 2 nodes to ensure redundancy of data in the event that a node becomes inoperable.  Each node can be placed in different geographical locations, but each must have access to all other nodes in the cluster.</p>
        <MadCap:snippetBlock src="../Connectors/AddNode.flsnp" />
        <h1><a name="Creating2"></a>Creating a VERA Share for SMB</h1>
        <h2>Establishing an SMB Credential on the Share Connector</h2>
        <ol>
            <li>Enter the IP&#160;address for your Share Connector into your web browser.</li>
            <li>Log in using the administrator credentials you set during the installation.</li>
            <li>Click <span class="UI">Share Credentials</span>.</li>
        </ol>
        <p class="listcont">
            <img src="../../Images/Connectors/smbCred.png" MadCap:mediastyle="@media print { max-width: 5in; }" style="width: 519px;height: 161px;" />
        </p>
        <ol MadCap:continue="true">
            <li>In the SMB page, click <span class="UI">Add New</span>.</li>
        </ol>
        <p class="listcont">
            <img src="../../Images/Connectors/SMBCredAdd.png" MadCap:mediastyle="@media print { max-width: 5in; }" style="width: 521px;height: 210px;" />
        </p>
        <ol MadCap:continue="true">
            <li>For <span class="UI">SMB Name</span>, assign a name that identifies the purpose of this credential.</li>
        </ol>
        <p class="listcont">This name will display in a drop-down list on your VERA tenant.</p>
        <ol MadCap:continue="true">
            <li>For <span class="UI">User Name</span>, enter the user name for an SMB administrator.</li>
            <li>For <span class="UI">Domain</span>, enter the domain associated with the user name you entered.</li>
            <li>For <span class="UI">Password</span>, enter the password associated with the user name you entered.</li>
            <li>Click <span class="UI">Create</span>.</li>
        </ol>
        <h2>Adding a Share Record on the VERA Tenant</h2>
        <ol start="1">
            <li>In the VERA <MadCap:glossaryTerm glossTerm="NewGlossary.Term5">Admin Portal</MadCap:glossaryTerm>, g​o  to <span class="UI">Settings &gt; Shares</span>.</li>
        </ol>
        <p class="listcont">
            <img src="../../Images/Integrations/ShareGetStarted.png" style="width: 563px;height: 316px;" />
        </p>
        <ol MadCap:continue="true">
            <li>Click <span class="UI">Get Started</span>.</li>
        </ol>
        <p class="listcont">If you have already configured a share, you won't see a Get Started button. Click Add, instead.​</p>
        <p class="listcont">
            <img src="../../Images/Connectors/ShareAccount94.png" MadCap:mediastyle="@media print { max-width: 4in; }" style="width: 474px;height: 255px;" />
        </p>
        <ol MadCap:continue="true">
            <li>Click the SMB icon.</li>
            <li>Click <span class="UI">Next</span>.</li>
        </ol>
        <p class="listcont">
            <img src="../../Images/smb.bmp" />
        </p>
        <ol MadCap:continue="true">
            <li>For <span class="UI">Server Location</span>, enter the host name or IP address for your SMB.</li>
            <li>For <span class="UI">Share Name</span>, enter the name of the share as defined on the SMB server.</li>
            <li>For <span class="UI">Display Name</span>, enter the string to use in VERA to refer to this share.</li>
            <li>For <span class="UI">SMB Credential</span>, select the credential you created on the Share Connector.</li>
        </ol>
        <ol MadCap:continue="true">
            <li>Under <span class="UI">Connector DNS Name</span>, select the Connector to assign to this Share.</li>
        </ol>
        <p class="listcont">If your deployment includes a Share Connector, it should already be installed. If you are not using a Share Connector for this Share, select None.</p>
        <ol MadCap:continue="true">
            <li>Click <span class="UI">Complete</span>.</li>
        </ol>
        <p class="listcont">
            <img src="../../Images/SMBShareBlank2-96.png" MadCap:mediastyle="@media print { max-width: 6in; }" style="width: 619px;height: 294px;" />
        </p>
        <ol MadCap:continue="true">
            <li>In the <span class="UI">Folder</span> field, enter the path to the subfolder in which the files to be secured are located.</li>
            <li>(Optional) For default user, enter the user to set as the default owner for any file that does not have an owner.</li>
        </ol>
        <p class="listcont">For example, if a user leaves the company and is removed from AD, the files owned by that user will need a new owner. </p>
        <ol MadCap:continue="true">
            <li>Under <span class="UI">Advanced Settings</span>, define the domain to apply when the SMB permissions specify Everyone.</li>
            <li>Select which VERA permission you want to apply to each SMB permission.</li>
            <li>Click <span class="UI">Save</span>. </li>
        </ol>
        <h2>When You Need More Than One Share</h2>
        <MadCap:snippetBlock src="../Connectors/MultiShare.flsnp" />
        <h1>Installing and Configuring an AD&#160;Connector</h1>
        <h2>Deploying the OVA Template</h2>
        <MadCap:snippetBlock src="../Connectors/ConnectorOVATask.flsnp" />
        <h2>Configuring the Base Connector</h2>
        <MadCap:snippetBlock src="../Connectors/ConnectorsBaseTask.flsnp" />
        <h2>Configuring the AD&#160;Connector Settings</h2>
        <ol>
            <li>Select <span class="UI">AD Connector</span>.</li>
            <li>Click on the <span class="UI">Get Keys</span> button.</li>
        </ol>
        <p class="listcont">Note: This step should launch the <MadCap:glossaryTerm glossTerm="NewGlossary.Term5">Admin Portal</MadCap:glossaryTerm> for your VERA tenant. If it doesn’t launch, enter the tenant URL in a separate browser window.</p>
        <ol MadCap:continue="true">
            <li>Log into your VERA admin portal with administrator credentials.</li>
            <li>In the cloud VERA admin portal, select <span class="UI">Connectors</span>.</li>
        </ol>
        <p class="listcont">
            <img src="../../Images/Connectors/ConnectorAfterKey.png" style="width: 469px;height: 321px;" />
        </p>
        <ol MadCap:continue="true">
            <li>Click on the “gear” icon at top right of screen.</li>
        </ol>
        <p>
            <img src="../../Images/Connectors/ConnectorKeyList2.png" style="width: 746px;height: 150px;" MadCap:mediastyle="@media print { max-width: 5in; }" />
        </p>
        <ol MadCap:continue="true">
            <li>Select the <span class="UI">AD Connector Key</span> option.</li>
            <li>Copy the displayed AD Connector key.</li>
            <li>Return to the AD Connector browser screen.</li>
            <li>Paste in the key.</li>
            <li>Click <span class="UI">Next</span>.</li>
        </ol>
        <p class="listcont">
            <img src="../../Images/Connectors/ADConfig90.png" MadCap:mediastyle="@media print { max-width: 5in; }" style="width: 542px;height: 417px;" />
        </p>
        <ol MadCap:continue="true">
            <li>Enable use of the VERA AD Connector:</li>
        </ol>
        <ol style="list-style-type: lower-alpha;">
            <li class="level2">AD Server Hostname: Enter the hostname of the server on which your Active Directory system is installed.</li>
            <li class="level2">Port: Enter the port on which AD is listening.</li>
            <li class="level2">Enable SSL: Select if the AD server requires secure SSL connections (LDAPS).</li>
            <li class="level2">Require LDAPS certificate to be signed by a trusted Certificate Authority: Select to disallow self-signed certificates.</li>
            <li class="level2">Enable Distribution Group Support: Select to cause VERA to assess an email address for use as a distribution list. If the email address does identify a list of other email addresses, VERA creates a membership list associated with the distribution group. Only known VERA users are added to the membership list. </li>
            <li class="level2">Base DN: Enter the location to search for users and groups in Active Directory. Use LDAP format. For example, if the domain components are mydomain and com, you would enter <span class="entry">dc=mydomain, dc=com</span>.</li>
            <li class="level2">User Name: Enter a service account having Read privileges for your Active Directory system. </li>
            <li class="level2">Password: Enter the password for the user name you just entered.</li>
            <li class="level2">Advanced Settings: Click to specify separate base DNs for users and groups if you have configured your underlying directory to require separation. Leaving either or both of the fields blank causes VERA to use the Base DN.</li>
            <li class="level2">Click <span class="UI">Complete</span>.</li>
        </ol>
        <ol start="12">
            <li>Confirm the configuration:</li>
        </ol>
        <ol style="list-style-type: lower-alpha;">
            <li class="level2">Make sure the Connector you just configured displays in the <span class="UI">Server Farm</span> page under <span class="UI">Nodes</span>.</li>
        </ol>
        <p class="listcont">
            <img src="../../Images/Connectors/ConnectorConfirm.png" alt="" title="" style="width: 497px;height: 349px;" MadCap:mediastyle="@media print { max-width: 4in; }" />
        </p>
        <ol style="list-style-type: lower-alpha;" MadCap:continue="true">
            <li class="level2">In your VERA tenant Admin Portal, make sure you see a node in the Connectors server farm.</li>
            <li class="level2">Confirm that the administrator has received an email with “ADC is UP” in the subject.</li>
        </ol>
        <ol start="13">
            <li>Configure AD Connector authentication in your VERA tenant Admin Portal. See <a href="#Setting2">Setting Up the AD Connector</a>.</li>
        </ol>
        <h2>High Availability: Creating a VERA AD Connector Cluster</h2>
        <p>As part of planning and building out your VERA deployment to obtain high availability and scalability, VERA recommends creating a cluster of 2 nodes (VERA AD Connectors).  The existence of a cluster ensures configuration data is replicated across the nodes within the cluster.  Additional nodes can also be added to increase scale as needed.</p>
        <MadCap:snippetBlock src="../Connectors/AddNode.flsnp" />
        <h1><a name="Testing"></a>Testing an SMB Integration</h1>
        <p>To confirm that your integration is properly configured, you should test each aspect of the configuration. Following are sample test cases for the following deployment:</p>
        <p>
            <img src="../../Images/SMBExample.png" MadCap:mediastyle="@media print { max-width: 3in; }" style="width: 472px;height: 417px;" />
        </p>
        <h2>Confirm that Files Added to the SMB Folder Are Secured</h2>
        <ol>
            <li>Have Deb drag a Word file into the /TopSecret folder.</li>
            <li>Confirm that the file is converted to HTML, indicating it has been <MadCap:glossaryTerm glossTerm="NewGlossary.Term1">secured</MadCap:glossaryTerm>. </li>
        </ol>
        <h2>Confirm that Users Have Expected Access to Secure Files</h2>
        <ol>
            <li>Have Ken drag a secure file from the SMB folder to a folder on another drive. </li>
            <li>Confirm that Ken can view the file, but not print it.</li>
        </ol>
        <h2>Confirm that Access is Revoked</h2>
        <ol>
            <li>From the SMB, remove Ken's access to /TopSecret.</li>
            <li>Confirm that Ken can no longer view the secure file.</li>
        </ol>
        <h1><a name="Monitori"></a>Monitoring Health</h1>
        <MadCap:snippetBlock src="../Connectors/ConnectorsMonitorTask.flsnp" />
    </body>
</html>