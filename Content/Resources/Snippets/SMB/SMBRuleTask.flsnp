﻿<?xml version="1.0" encoding="utf-8"?>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd" MadCap:lastBlockDepth="5" MadCap:lastHeight="2080" MadCap:lastWidth="873">
    <head>
        <link href="../../TableStyles/KBTable1.css" rel="stylesheet" MadCap:stylesheetType="table" />
    </head>
    <body>
        <h2>SMB&#160;Rules: Watch It</h2>
        <p>
            <img src="../../Images/AutoSecureSMB.png" style="width: 300px;height: auto;" />
        </p>
        <h2>SMB Rules: Read It</h2>
        <p>To create an SMB&#160;rule:</p>
        <ol>
            <li class="level2">In the Admin Portal, go to <span class="UI">Settings &gt; Rules</span>.</li>
        </ol>
        <p class="listcont">
            <img src="../../Images/Integrations/RulesGetStarted.png" style="width: 638px;height: 318px;" />
        </p>
        <ol MadCap:continue="true">
            <li>Click <span class="UI">Get Started</span>. </li>
        </ol>
        <p class="listcont">If this is not your first rule, Get Started won't be displayed. Click Create Rule, instead. </p>
        <p class="listcont">
            <img src="../../Images/Integrations/CreateRule.png" style="width: 375px;height: 179px;" />
        </p>
        <ol MadCap:continue="true">
            <li>Select the type of rule (SMB).</li>
            <li>Enter a name that identifies the rule.</li>
            <li>Click <span class="UI">Create</span>.</li>
        </ol>
        <p class="listcont">
            <img src="../../Images/SMBRuleBlank.png" MadCap:mediastyle="@media print { max-width: 5in; }" style="width: 589px;height: 318px;" />
        </p>
        <p>&#160;</p>
        <ol MadCap:continue="true">
            <li>Build your filter rule:</li>
        </ol>
        <table style="mc-table-style: url('../../TableStyles/KBTable1.css');width: 80%;" class="TableStyle-KBTable1" cellspacing="0">
            <col class="TableStyle-KBTable1-Column-Regular" style="width: 116px;" />
            <col class="TableStyle-KBTable1-Column-Regular" style="width: 114px;" />
            <col class="TableStyle-KBTable1-Column-Regular" />
            <col class="TableStyle-KBTable1-Column-Regular" />
            <col class="TableStyle-KBTable1-Column-Regular" />
            <tbody>
                <tr class="TableStyle-KBTable1-Body-LightRows">
                    <th class="TableStyle-KBTable1-BodyE-Regular-LightRows">Item</th>
                    <th class="TableStyle-KBTable1-BodyE-Regular-LightRows">Operator</th>
                    <th class="TableStyle-KBTable1-BodyE-Regular-LightRows">Operator</th>
                    <th class="TableStyle-KBTable1-BodyE-Regular-LightRows">Item</th>
                    <th class="TableStyle-KBTable1-BodyD-Regular-LightRows">Item</th>
                </tr>
                <tr class="TableStyle-KBTable1-Body-DarkerRows">
                    <td class="TableStyle-KBTable1-BodyE-Regular-DarkerRows">Folder Path</td>
                    <td rowspan="4" class="TableStyle-KBTable1-BodyE-Regular-DarkerRows">Does/Does Not</td>
                    <td class="TableStyle-KBTable1-BodyE-Regular-DarkerRows">Starts With / Ends With / Contains</td>
                    <td class="TableStyle-KBTable1-BodyE-Regular-DarkerRows">Documents / Pictures / Music / Videos / Desktop</td>
                    <td class="TableStyle-KBTable1-BodyD-Regular-DarkerRows">Sub folders (Required)</td>
                </tr>
                <tr class="TableStyle-KBTable1-Body-LightRows">
                    <td class="TableStyle-KBTable1-BodyE-Regular-LightRows">Sub Folder</td>
                    <td class="TableStyle-KBTable1-BodyE-Regular-LightRows">Starts With / Ends With / Contains</td>
                    <td class="TableStyle-KBTable1-BodyE-Regular-LightRows">&#160;</td>
                    <td class="TableStyle-KBTable1-BodyD-Regular-LightRows">&#160;</td>
                </tr>
                <tr class="TableStyle-KBTable1-Body-DarkerRows">
                    <td class="TableStyle-KBTable1-BodyE-Regular-DarkerRows">Secure By</td>
                    <td class="TableStyle-KBTable1-BodyE-Regular-DarkerRows">Starts With / Ends With / Contains / Equals / Member Of</td>
                    <td class="TableStyle-KBTable1-BodyE-Regular-DarkerRows">user name</td>
                    <td class="TableStyle-KBTable1-BodyD-Regular-DarkerRows">n/a</td>
                </tr>
                <tr class="TableStyle-KBTable1-Body-LightRows">
                    <td class="TableStyle-KBTable1-BodyE-Regular-LightRows">File Name</td>
                    <td class="TableStyle-KBTable1-BodyE-Regular-LightRows">Starts With / Ends With / Contains / Equals</td>
                    <td class="TableStyle-KBTable1-BodyE-Regular-LightRows">file name</td>
                    <td class="TableStyle-KBTable1-BodyD-Regular-LightRows">n/a</td>
                </tr>
                <tr class="TableStyle-KBTable1-Body-DarkerRows" MadCap:conditions="">
                    <td class="TableStyle-KBTable1-BodyE-Regular-DarkerRows">Created Date</td>
                    <td class="TableStyle-KBTable1-BodyE-Regular-DarkerRows" rowspan="2" colspan="2">Older Than / Newer Than</td>
                    <td class="TableStyle-KBTable1-BodyD-Regular-DarkerRows" colspan="2" rowspan="2">Date</td>
                </tr>
                <tr class="TableStyle-KBTable1-Body-LightRows" MadCap:conditions="">
                    <td class="TableStyle-KBTable1-BodyB-Regular-LightRows">Modified Date</td>
                </tr>
            </tbody>
        </table>
        <ol MadCap:continue="true">
            <li>(Optional) For <span class="UI">Give access to</span>, select to define users with access and which policies to apply.</li>
            <li>(Optional) For <span class="UI">Allow users to share the file with others</span>, select to enable file sharing and which policy to apply.</li>
            <li>(Optional) For <span class="UI">Enforce rule even when manual securing is disabled</span>, select to apply the rule for users who do not have permission to secure files.</li>
            <li>Click&#160;<span class="UI">Save Rule</span>.</li>
            <li>If you haven't created a Share yet for your SMB, you'll need to do that before your rule will work.</li>
        </ol>
    </body>
</html>